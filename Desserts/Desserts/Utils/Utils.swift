//
//  Utils.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Foundation

enum EndPoints {
    static let dessertList = "https://themealdb.com/api/json/v1/1/filter.php?c=Dessert"
    static let dessertDetail = "https://themealdb.com/api/json/v1/1/lookup.php?i=%@"
}

enum RequestType {
    case List
    case Detail(String)
}

enum AppErrors: Error {
    case invalidURL
    case invalidResponse
}
