//
//  Repository.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Combine
import Foundation

protocol RepositoryContract: AnyObject {
    func fetch<T:Codable>(_ requestType: RequestType) -> AnyPublisher<T, Error>
}

final class Repository: RepositoryContract {
    func fetch<T:Codable>(_ requestType: RequestType) -> AnyPublisher<T, Error> {
        var endPoint: String
        switch requestType {
        case .List:
            endPoint = EndPoints.dessertList
        case .Detail(let id):
            endPoint = String(format: EndPoints.dessertDetail, id)
        }
        guard let url = URL(string: endPoint) else {
            return Fail(outputType: T.self, failure: AppErrors.invalidURL).eraseToAnyPublisher()
        }
        return URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { error in
                return AppErrors.invalidResponse
            }
            .eraseToAnyPublisher()
    }
}
