//
//  DessertsApp.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import SwiftUI

@main
struct DessertsApp: App {
    var body: some Scene {
        WindowGroup {
            let repository = Repository()
            let viewModel = ListViewModel(repository: repository)
            MealsList(viewModel: viewModel)
        }
    }
}
