//
//  MealsList.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Foundation
import SwiftUI

struct MealsList: View {
    @ObservedObject var viewModel: ListViewModel
    
    var body: some View {
        NavigationView {
            List(viewModel.meals) { meal in
                NavigationLink {
                    let viewModel = DetailViewModel(meal: meal)
                    MealDetail(viewModel: viewModel)
                } label: {
                    VStack {
                        AsyncImage(url: meal.thumb)
                            .frame(maxWidth: .infinity,
                                   maxHeight: 280)
                            .cornerRadius(1)
                        Text(meal.name)
                            .font(.headline)
                        
                    }
                    .padding(.vertical)
                }
            }
            .navigationTitle("Desserts")
            .navigationBarTitleDisplayMode(.inline)
        }
        .onAppear {
            viewModel.fetch()
        }
    }
}

struct MealsList_Preview: PreviewProvider {
    static var previews: some View {
        let repository = Repository()
        let viewModel = ListViewModel(repository: repository)
        MealsList(viewModel: viewModel)
    }
}
