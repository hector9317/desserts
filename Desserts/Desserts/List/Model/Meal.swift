//
//  Meal.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Foundation

struct MealResponse: Codable {
    let meals: [Meal]
}

struct Meal: Identifiable, Codable {
    let id: String
    let name: String
    let thumb: URL
    
    enum CodingKeys: String, CodingKey {
        case id = "idMeal"
        case name = "strMeal"
        case thumb = "strMealThumb"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.thumb = try container.decode(URL.self, forKey: .thumb)
    }
    
    init(id: String, name: String, thumb: URL) {
        self.id = id
        self.name = name
        self.thumb = thumb
    }
}
