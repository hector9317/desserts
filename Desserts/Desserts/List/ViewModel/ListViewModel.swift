//
//  ListViewModel.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Combine
import Foundation

final class ListViewModel: ObservableObject {
    @Published private(set) var meals = [Meal]()
    private let repository: RepositoryContract
    private var cancellable: AnyCancellable?
    
    init(repository: RepositoryContract) {
        self.repository = repository
    }
    
    func fetch() {
        cancellable = repository.fetch(.List)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
            }, receiveValue: { [weak self] (response: MealResponse) in
                self?.meals = response.meals
            })
    }
}
