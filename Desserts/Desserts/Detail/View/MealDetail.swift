//
//  MealDetail.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Foundation
import SwiftUI

struct MealDetail: View {
    @ObservedObject var viewModel: DetailViewModel

    var body: some View {
        ScrollView {
            LazyVStack(alignment: .leading) {
                Text(viewModel.dataViewModel.name ?? "")
                    .font(.title)
                Text("Tags: \(viewModel.dataViewModel.tags ?? "")")
                    .font(.headline)
                Text(viewModel.dataViewModel.area ?? "")
                    .font(.subheadline)
                Text("Instructions:")
                    .fontWeight(.semibold)
                    .font(.caption)
                    .padding(.top)
                Text(viewModel.dataViewModel.instructions ?? "")
                    .font(.caption2)
                Text("Ingredients:")
                    .fontWeight(.semibold)
                    .font(.caption)
                    .padding(.top)
                Text(viewModel.dataViewModel.ingredients)
                    .font(.caption2)
                Button {
                    if let youtube = viewModel.dataViewModel.youtube,
                       let url = URL(string: youtube) {
                        UIApplication.shared.open(url)
                    }
                } label: {
                    Image("youtube")
                }
            }
            .padding()
        }
        .onAppear {
            viewModel.fetch()
        }
    }
}

struct MealDetail_Previews: PreviewProvider {
    static var previews: some View {
        let meal = Meal(id: "52893",
                        name: "Apple & Blackberry Crumble",
                        thumb: URL(string: "https://www.themealdb.com/images/media/meals/adxcbq1619787919.jpg")!)
        MealDetail(viewModel: DetailViewModel(meal: meal))
    }
}
