//
//  MealDataViewModel.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Combine
import Foundation

final class MealDataViewModel: ObservableObject {
    private let data: [String: String?]
    var name: String? {
        data["strMeal"] ?? ""
    }
    var area: String? {
        data["strArea"] ?? ""
    }
    var instructions: String? {
        data["strInstructions"] ?? ""
    }
    var thumb: String? {
        data["strMealThumb"] ?? ""
    }
    var tags: String? {
        data["strTags"] ?? ""
    }
    var youtube: String? {
        data["strYoutube"] ?? ""
    }
    var ingredients: String {
        var ingredients = [String?]()
        var measures = [String?]()
        var result = [String]()
        for i in 1 ... 15 {
            let keyIngredient = "strIngredient\(i)"
            if let value = data[keyIngredient], value != nil, value != "" {
                ingredients.append(value)
            }
            let keyMeasure = "strMeasure\(i)"
            if let value = data[keyMeasure], value != nil, value != "" {
                measures.append(value)
            }
        }
        for (ingredient, measure) in zip(ingredients.compactMap({ $0 }), measures.compactMap({ $0 })) {
            result.append(ingredient + " - " + measure)
        }
        return result.joined(separator: "\n")
    }
    
    init(data: [String : String?]) {
        self.data = data
    }
}

