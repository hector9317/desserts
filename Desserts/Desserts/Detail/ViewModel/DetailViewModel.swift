//
//  DetailViewModel.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Combine
import Foundation

final class DetailViewModel: ObservableObject {
    @Published private(set) var details = [[String: String?]]()
    @Published private(set) var dataViewModel: MealDataViewModel
    private var repository: RepositoryContract
    private var cancellable: AnyCancellable?
    private(set) var meal: Meal
    
    init(meal: Meal, repository: RepositoryContract = Repository()) {
        self.repository = repository
        self.meal = meal
        self.dataViewModel = MealDataViewModel(data: [:])
    }
    
    func fetch() {
        cancellable = repository.fetch(.Detail(meal.id))
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
            }, receiveValue: { [weak self] (response: DetailResponse) in
                if let data = response.meals.first {
                    self?.dataViewModel = MealDataViewModel(data: data)
                }
                self?.details = response.meals
            })
    }
}
