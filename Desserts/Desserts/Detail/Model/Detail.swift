//
//  Detail.swift
//  Desserts
//
//  Created by Hector Bautista on 17/09/23.
//

import Foundation

struct DetailResponse: Codable {
    let meals: [[String: String?]]
}
